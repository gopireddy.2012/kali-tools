---
Title: dfdatetime
Homepage: https://github.com/log2timeline/dfdatetime
Repository: https://salsa.debian.org/pkg-security-team/dfdatetime
Architectures: all
Version: 20210509-1
Metapackages: kali-linux-everything kali-tools-forensics 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### python3-dfdatetime
 
  dfDateTime, or Digital Forensics date and time, provides date and
  time objects to preserve accuracy and precision.
 
 **Installed size:** `198 KB`  
 **How to install:** `sudo apt install python3-dfdatetime`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 {{< /spoiler >}}
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
